from random import randint

name = input("Hi! What is your name?")
guess_number = 1

while guess_number < 6:
    birthmonth = randint(1,12)
    birthyear = randint(1990, 2022)
    print(name + ", were you born in " + str(birthmonth) + "/" + str(birthyear) + "?")
    response = input("Yes or No?")
    if response == "Yes":
        print("I knew it!")
        exit()
    elif response == "No" and guess_number < 5:
        print("Drat! Lemme try again!")
        guess_number = guess_number + 1
    else:
        print("I have other things to do. good bye.")
        exit()

# stretch goals
# instead of yes or no, reply with yes, later, and earlier
# have response affect future guesses
# add day to the guess
# have program print month name instead of number
# Guess 1: Syed, were you bron in January 1996
if birthmonth == 2:
    birthday = randint(1,28)
elif birthmonth == 